package com.example.mrwang.qrimagetool;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView mQr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mQr = (ImageView) findViewById(R.id.img_qr);
        Bitmap bitmap = QRImageTool.getQRCOde("http://www.baidu.com",240, 240);
        mQr.setImageBitmap(bitmap);
        mQr.setBackgroundColor(Color.WHITE);
    }
}
